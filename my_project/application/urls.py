from django.urls import path, re_path
from .views import *

urlpatterns = [
    path('', ApplicationHome.as_view(), name='home'),
    path('registration/', RegistrationUser.as_view(), name='registration'),
    path('login/', LoginUser.as_view(), name='login'),
    path('logout/', logout_user, name='logout'),
    path('users/', UsersShow.as_view(), name='users'),
    path('delete_user/<slug:username>', delete_user, name='delete_user'),
    path(r'^block_user/(?P<username>)/(?P<flag>)/$', block_user, name='block_user'),
    path('addimage/', AddImage.as_view(), name='addimage'),
    path('myrecords/', ShowMyRecords.as_view(), name='myrecords'),
    path(r'^delete_record/(?P<rec_id>)/(?P<url>)/$', delete_record, name='delete_record'),
    path('show_record/<int:rec_id>', show_record, name='show_record'),
]