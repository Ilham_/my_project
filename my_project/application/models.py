from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User


class Images(models.Model):

    photo = models.ImageField(upload_to="photos/%Y/%m/%d/", verbose_name="Изображение")
    tag = models.CharField(max_length=255, verbose_name="Тег")
    slug = models.SlugField(max_length=255, unique=True, db_index=True, verbose_name="URL")
    time_create = models.DateTimeField(auto_now_add=True, verbose_name="Время создания")
    time_update = models.DateTimeField(auto_now=True, verbose_name="Время изменения")

    # добавление внешнего ключа
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name="Пользователи")

    def get_absolute_url(self):
        return reverse('images', kwargs={'images_slug': self.slug})