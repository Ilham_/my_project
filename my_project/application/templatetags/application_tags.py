from django import template  # Для работы с шаблонами
from application.models import *

register = template.Library()


@register.inclusion_tag("application/main_menu.html")
def show_main_menu(menu, request):
    return {"menu": menu, 'request': request}
