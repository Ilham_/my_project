from django.contrib.auth import logout, login
from django.contrib.auth.views import LoginView
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView
import datetime

from application.models import *
from .forms import *
from .utils import *

menu = [
    {'title': "Главная страница", 'url_name': 'home'},
    # {'title': "Регистрация", 'url_name': 'registration'},
    # {'title': "Войти", 'url_name': 'login'},
    # {'title': "Войти", 'url_name': 'logout'},
]


class UsersShow(DataMixin, ListView):
    model = User

    template_name = 'application/users.html'
    context_object_name = 'users'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(title='Пользователи сайта')
        return dict(list(context.items()) + list(c_def.items()))

    def get_queryset(self):
        return User.objects.filter(is_superuser=False)


class RegistrationUser(DataMixin, CreateView):
    form_class = RegisterUserForm

    template_name = 'application/registration.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(title='Регистрация')
        return dict(list(context.items()) + list(c_def.items()))

    def form_valid(self, form):
        user = form.save()
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            return redirect('users')
        else:
            login(self.request, user)
            return redirect('home')


class LoginUser(DataMixin, LoginView):

    form_class = LoginUserForm
    template_name = 'application/login.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(title='Авторизация')
        return dict(list(context.items()) + list(c_def.items()))


class AddImage(DataMixin, CreateView):

    form_class = AddImageForm
    template_name = 'application/addimage.html'
    success_url = reverse_lazy('home')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(title='Добавление изображения')
        return dict(list(context.items()) + list(c_def.items()))

    def form_valid(self, form):
        username = self.request.user.username
        now = datetime.datetime.now()

        self.object = form.save(commit=False)
        self.object.slug = username + '_' + now.strftime("%d_%m_%Y_%H_%M_%S")
        self.object.user_id = User.objects.get(username=username).pk
        self.object.save()

        return redirect('home')


class ApplicationHome(DataMixin, ListView):

    model = Images

    template_name = 'application/index.html'
    context_object_name = 'records'

    def get_context_data(self, *, object_list=None, **kwargs):
        images_all = Images.objects.all()
        context = super().get_context_data(**kwargs)
        context['records'] = images_all
        context['users'] = User
        c_def = self.get_user_context(title='Мой проект')

        return dict(list(context.items()) + list(c_def.items()))


class ShowMyRecords(DataMixin, ListView):

    model = Images

    template_name = 'application/records.html'
    context_object_name = 'records'

    def get_context_data(self, *, object_list=None, **kwargs):
        user_id = User.objects.get(username=self.request.user.username)
        images_all = Images.objects.filter(user_id=user_id)
        context = super().get_context_data(**kwargs)
        context['records'] = images_all
        c_def = self.get_user_context(title='Мои записи')

        return dict(list(context.items()) + list(c_def.items()))


def logout_user(request):
    print(request.user.username)
    logout(request)
    return redirect('home')


def delete_user(request, username):
    buf_user = User.objects.filter(username=username)
    buf_user.delete()
    return redirect('users')


def block_user(request, username, flag):
    buf_user = User.objects.get(username=username)

    if int(flag) == 1:
        buf_user.is_active = False
    else:
        buf_user.is_active = True

    buf_user.save()
    return redirect('users')


def delete_record(request, rec_id, url):
    rec = Images.objects.get(id=rec_id)
    rec.delete()
    return redirect(url)


def show_record(request, rec_id):
    record = Images.objects.get(id=rec_id)

    context = {
        'menu' : menu,
        'record': record,
    }

    return render(request, 'application/show_record.html', context=context)