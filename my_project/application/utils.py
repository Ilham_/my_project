from .models import *

menu = [
    {'title': "Главная страница", 'url_name': 'home'},
    # {'title': "Регистрация", 'url_name': 'registration'},
    # {'title': "Войти", 'url_name': 'login'},
]


class DataMixin:

    def get_user_context(self, **kwargs):
        context = kwargs
        context['menu'] = menu

        return context